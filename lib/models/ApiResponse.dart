enum Status { SUCCESS, ERROR, LOADING }

class ApiResponse<T> {
  T body;
  Status status;
  String errorMessage;

  ApiResponse(
      {required this.body,
      this.status = Status.LOADING,
      this.errorMessage = ''});
}
