import 'package:cloud_firestore/cloud_firestore.dart';

class Ingredient {
  String? id;
  String? name;
  String? category;
  int? price;
  String? imageUrl;
  int? quantity;

  Ingredient({
    required this.id,
    required this.name,
    required this.category,
    required this.price,
    required this.imageUrl,
    required this.quantity,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'category': category,
      'price': price,
      'imageUrl': imageUrl,
      'quantity': quantity,
    };
  }

  Ingredient.fromJson(Map<String, dynamic> MealPlanMap)
      : id = MealPlanMap["id"],
        name = MealPlanMap["name"],
        category = MealPlanMap["category"],
        price = MealPlanMap["price"],
        imageUrl = MealPlanMap["imageUrl"],
        quantity = MealPlanMap["quantity"];

  Ingredient.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    id = documentSnapshot.id;
    name = documentSnapshot.get('name');
    category = documentSnapshot.get('category');
    price = documentSnapshot.get('price');
    imageUrl = documentSnapshot.get('imageUrl');
    quantity = documentSnapshot.get('quantity');
  }
}
